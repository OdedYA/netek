import * as React from 'react';
import  Header from './components/header';
import Main from './components/main';
import Footer from './components/footer';
import './App.css';

class App extends React.Component {
  render() {
    let mystring: string;
    mystring = 'test';

    return (
      <div className="App">
        <header className="App-header">
          <Header name="Header" />
          <h1 className="App-title">Welcome to React {mystring}</h1>
        </header>
        <main>
          <Main name="Main" />
        </main>
        <footer>
          <Footer />
        </footer>
       
      </div>
    );
  }
}

export default App;
