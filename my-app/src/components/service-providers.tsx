import * as React from 'react';
import ServiceProvider from './service-provider';

function ServiceProviders() {
   return(
       <div>
          <ServiceProvider text="text" title="title" smallImage="smallImage" largeImage="largeImage"  />
          <ServiceProvider text="text" title="title" smallImage="smallImage" largeImage="largeImage"  />
          <ServiceProvider text="text" title="title" smallImage="smallImage" largeImage="largeImage"  />
       </div>
   );
}

export default ServiceProviders; 