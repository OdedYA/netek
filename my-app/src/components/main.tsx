import * as React from 'react';
import ServiceProviders from './service-providers';
import Form from './form';

export interface Props {
    name: string;
  }

function Main({name}:  Props) {
   return(
       <div>
           <ServiceProviders />
           <Form />
       </div>

   );
}

export default Main; 