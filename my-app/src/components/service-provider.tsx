import * as React from 'react';

export interface Props {
    text: string;
    title: string;
    smallImage: string;
    largeImage: string;
  }

function ServiceProvider({text, smallImage, title, largeImage}:  Props) {
   return(
       <div>
           <img src={smallImage} alt={title} />
           <div>{text}</div>
       </div>
   );
}

export default ServiceProvider; 