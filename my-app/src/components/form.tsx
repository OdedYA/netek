import * as React from 'react';

export interface Props {
   
}

class Form extends React.Component {
    render() {
        return (
        <div>
            <div className="Info Step" >
                        <div className="Count">
                            <img 
                                src="images/steps_icon_02.jpg" 
                                alt="שלב שני" 
                            />
                            <h2>מלאו את פרטי החשבון ממנו אתם מעוניינים להתנתק</h2>
                        </div>
                        <p><b>טופס פרטי בעל החשבון</b></p>
                        <table>
                            <tbody>
                                <tr>
                                <td><span>*</span>סוג חשבון:</td>
                                <td>
                                    <input type="radio" /> פרטי&nbsp;&nbsp;
                                    <input type="radio" name="AccountType" value="Company" /> עסקי
                                </td>                              
                            </tr>
                        </tbody></table>
                        <table id="CompanyInfo">
                            <tbody><tr>
                                <td><span>*</span>שם חברה:</td>
                                <td><input type="text" name="MyCompanyName" id="MyCompanyName" /></td>
                                <td><img className="validation" id="val_MyCompanyName" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>ח.פ. / ע.מ.:</td>
                                <td><input type="text" name="MyCompanyID" id="MyCompanyID" /></td>
                                <td><img className="validation" id="val_MyCompanyID" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>איש קשר:</td>
                                <td><input type="text" name="ContactName" id="ContactName" /></td>
                                <td><img className="validation" id="val_ContactName" /></td>
                            </tr>
                        </tbody></table>
                        <table id="PersonalInfo">
                            <tbody><tr>
                                <td><span>*</span>שם פרטי:</td>
                                <td><input type="text" name="FirstName" id="FirstName" /></td>
                                <td><img className="validation" id="val_FirstName" src="images/free/v.jpg" /></td>
                            </tr>
                            <tr>
                                <td><span>*</span>שם משפחה:</td>
                                <td><input type="text" name="LastName" id="LastName" /></td>
                                <td>
                                    <img 
                                        className="validation" 
                                        id="val_LastName" 
                                        src="images/free/v.jpg" 
                                        alt="לא מולא השדה שם משפחה" 
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><span>*</span>ת.ז.:</td>
                                <td><input type="text" name="PersonalID" id="PersonalID"  /></td>
                                <td>
                                    <img 
                                        className="validation" 
                                        id="val_PersonalID" 
                                        src="images/free/v.jpg" 
                                        alt="מספר ת.ז. לא תקין" 
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>           
                </div>              
            </div>);
    }
}

export default Form;