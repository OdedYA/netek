import * as React from 'react';

function Footer() {
   return(    
        <div>
            <div>
            <a 
                href="/lp_yes.php?source=YesCol1Banner" 
                target="_blank" 
                data-name="Yes_mcann" 
                data-creative="310x210b" 
            >
                <img src="https://www.netek.co.il/banners/yes/310x210b.jpg" alt="yes" />
            </a>
            <a 
                href="/lp_yes_bm.php?source=YesUltimate" 
                target="_blank" 
                data-name="Yes_bimix" 
                data-creative="310x210" 
            >
                <img 
                    src="https://www.netek.co.il/banners/yes_bm/310x210b.jpg" 
                    alt="yes" 
                    width="310" 
                />
            </a>
            </div>
        </div>          
    );
}

export default Footer; 